import VueRouter from 'vue-router'

import Users from './scenes/Users.vue'
import Todos from './scenes/Todos.vue'
import UserForm from './components/UserForm.vue'

const routes = [{
  path: '',
  redirect: '/users'
}, {
  path: '/users',
  component: Users,
  children: [{
      path: ':id',
      component: UserForm
    }

  ]
}, {
  path: '/todos',
  component: Todos
}]


export const router = new VueRouter({
  routes,
  mode: 'history',
})
