import Vue from 'vue'
import Vuex from 'vuex';

export const INCREMENT = 'INCREMENT'
export const DECREMENT = 'DECREMENT'

import axios from 'axios'

Vue.use(Vuex)

export const counter = new Vuex.Store({
  // namespaced: true,
  state: {
    counter: 123,
    users:[]
  },
  mutations: {
    INCREMENT(state, payload = 1) {
      state.counter += payload
    },
    [DECREMENT](state, payload = 1) {
      state.counter -= payload
    },
  },
  getters: {
    // counter: state => state.counter,
  }
})

export const store = new Vuex.Store({
  modules: {
    counter: counter,
    counter2: counter,
  },
  state: {
    entities: {
      1: {
        id: 1,
        title: "Example from Store",
        completed: false
      }
    },
    todos: [1],
    favourites: [1],
  },
  mutations: {
    addTodo(state, todo) {
      Vue.set(state.entities, todo.id, todo)
      state.todos.push(todo.id)
    },
    toggleTodo(state, id) {
      const todo = state.entities[id]
      todo.completed = !todo.completed
    },
    deleteTodo(state, id) {
      state.todos = state.todos.filter(todo => todo.id != id);
    },
    favouriteTodo(state, id) {
      state.favourites.push(id);
    },
    unfavouriteTodo(state, id) {
      state.favourites.splice(state.favourites.indexOf(id), 1);
    },
    startLoading(state) {
      state.loading = true
    },
    loadUsers(state, users) {
      state.users = users
      state.loading = false
    },
    fetchError(state, error) {
      state.error = error
      state.loading = false
    }
  },
  actions: {
    createTodo({
      commit
    }, title) {
      commit('addTodo', {
        id: Date.now(),
        title
      });
    },
    // fetchTodos: Service.fetchUsers
    fetchUsers({
      commit
    }) {
      commit("startLoading")
      axios.get('http://localhost:3000/users').then(response => {
          commit("loadUsers", response.data)
        }, error =>
        commit("fetchError", error))
    }
  },
  getters: {
    todos: state => state.todos.map(id => state.entities[id]),
    favourites: state => state.favourites.map(id => state.entities[id]),
    users: state => state.users
  }
})

window['store'] = store
