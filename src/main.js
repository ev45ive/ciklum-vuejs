import Vue from 'vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import {
  store
} from './store'

import {
  router
} from './router'


Vue.use(VueRouter)
Vue.use(VueResource)
Vue.http.options.root = 'http://localhost:3000/';

new Vue({
  el: '#app',
  render: h => h(App),
  store: store,
  router
})
